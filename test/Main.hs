module Main where


-------------------------------------------------------------------------------
import           Data.Maybe
import           Data.Monoid
import qualified Data.Vector                          as V
import           Statistics.Sample
import           Test.Framework
import           Test.Framework.Providers.HUnit
import           Test.Framework.Providers.QuickCheck2
import           Test.HUnit
import           Test.QuickCheck
import           Test.QuickCheck.Property
-------------------------------------------------------------------------------
import           Data.StatVar
-------------------------------------------------------------------------------


main = defaultMain statTests

-------------------------------------------------------------------------------
statTests :: [Test.Framework.Test]
statTests =
  [ testProperty "welford's method is accurately implemented" prop_welfords
  , testProperty "combining StatVars keep variance correct" prop_variance_comb
  , testProperty "incremental construction through monoid works" prop_monoid_eq_listToStatVar
  , testProperty "statvar produces right results compared to statistics" prop_accuracy
  , testProperty "newStatVar works properly" prop_newVar
  ]


-------------------------------------------------------------------------------
smallDiff fraction a b = abs (a - b) <= (abs a * fraction)


-------------------------------------------------------------------------------
prop_welfords :: [Double] -> Property
prop_welfords xs = length xs > 1 ==> (whenFail' err $ smallDiff 0.00001 var' var'')
    where
      err = putStrLn $ "Statistics got variance " ++ show var' ++ " but we got " ++ show var''
      v = V.fromList xs
      st = fromJust $ listToStatVar xs
      var' = varianceUnbiased v
      var'' = svVariance st


-------------------------------------------------------------------------------
prop_variance_comb :: [Double] -> [Double] -> Property
prop_variance_comb xs ys =
    length xs > 1 && length ys > 1 ==> (whenFail' err $ smallDiff 0.00001 var' var'')
    where
      err = putStrLn $ "Full variance is " ++ show var' ++ " but combining got " ++ show var''
      st1 = fromJust $ listToStatVar xs
      st2 = fromJust $ listToStatVar ys
      stAll = fromJust $ listToStatVar $ xs ++ ys
      var' = svVariance stAll
      var'' = svVariance $ mappend st1 st2


-------------------------------------------------------------------------------
prop_monoid_eq_listToStatVar :: [Double] -> Property
prop_monoid_eq_listToStatVar xs =
    length xs > 1 ==> smallDiff 0.00001 (svVariance st1) (svVariance st2)
    where
      st1 = mconcat $ map newStatVar xs
      st2 = fromJust $ listToStatVar xs



prop_accuracy :: [Double] -> Property
prop_accuracy xs =
    V.length xs' > 0 ==>
      (svMax a) == (svMax b) &&
      (svMin a) == (svMin b) &&
      (svCount a) == (svCount b) &&
      smallDiff 0.001 (svMean a) (svMean b) &&
      smallDiff 0.001 (svStdDev a) (svStdDev b)
    where
      xs' = V.fromList xs
      a = mkStatVar xs'
      b = foldStatVar xs'


prop_newVar :: [Double] -> Property
prop_newVar xs =
    V.length xs' > 0 ==>
      (svMax a) == (svMax b) &&
      (svMin a) == (svMin b) &&
      (svCount a) == (svCount b) &&
      smallDiff 0.001 (svMean a) (svMean b) &&
      smallDiff 0.001 (svStdDev a) (svStdDev b)
    where
      xs' = V.fromList xs
      a = mconcat $ map newStatVar xs
      b = foldStatVar xs'

